import Cors from 'cors';

const cors = Cors({
	methods: ['GET', 'POST', 'HEAD', 'OPTIONS'],
	origin: '*',
});

const handleCors = (req, res, fn) => {
	return new Promise((resolve, reject) => {
		fn(req, res, (result) => {
			if (result instanceof Error) {
				return reject(result);
			}

			return resolve(result);
		});
	});
};

export default async (req, res) => {
	await handleCors(req, res, cors);
	console.log(req.cookies);
	res.status(200).json({ name: 'John Doe' });
};

export const config = {
	api: {
		bodyParser: true,
	},
};
