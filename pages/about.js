import React from 'react';
import Link from 'next/link';
import { promises as fs } from 'fs';
import path from 'path';

// const URL = 'https://opentdb.com/api.php?amount=10';
const URL = 'https://jsonplaceholder.typicode.com/posts';

const About = ({ posts, files }) => {
	// console.log({ posts, files });

	return (
		<div>
			<h3>HELLO ABOUT</h3>
			<ul>
				{posts.map((post) => (
					<li key={post.id}>
						<h3>
							<Link
								href={{
									pathname: '/posts/[id]',
									query: { id: post.id },
								}}
							>
								<a>
									{post.id} - {post.title}
								</a>
							</Link>
						</h3>
						<p>{post.body}</p>
					</li>
				))}
			</ul>
		</div>
	);
};

export const getStaticProps = async (context) => {
	const posts = await (await fetch(URL)).json();
	if (!posts) {
		return {
			redirect: {
				destination: '/',
				permanent: false,
			},
		};
	}

	const currentDir = process.cwd();
	const postDir = path.join(currentDir, '/pages/posts');
	const filesName = await fs.readdir(postDir);
	console.log({ currentDir, __dirname, postDir, filesName });

	const listFiles = filesName.map(async (fileName) => {
		const filePath = path.join(postDir, fileName);
		const fileContent = await fs.readFile(filePath, 'utf16le');

		return {
			id: filePath,
			fileName,
			content: fileContent,
		};
	});

	return {
		props: {
			posts,
			files: await Promise.all(listFiles),
		},
	};
};

export default About;
