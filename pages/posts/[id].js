import React from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';

const URL = 'https://jsonplaceholder.typicode.com/posts';

const Post = ({ posts, img }) => {
	const router = useRouter();

	return (
		<div>
			<button
				onClick={() => router.push('/about', undefined, { shallow: true })}
			>
				Back
			</button>
			<div>
				Hello Post: <b>{posts?.title}</b>
				{img?.status === 'success' && (
					<Image src={img?.message} width={500} height={500} alt="Dog Random" />
				)}
			</div>
		</div>
	);
};

// This function gets called at build time
export async function getStaticPaths() {
	// Call an external API endpoint to get posts
	const posts = await (await fetch(URL)).json();

	// Get the paths we want to pre-render based on posts
	const paths = posts.map((post) => `/posts/${post.id}`);
	console.log('[ID]: getStaticPaths');

	// We'll pre-render only these paths at build time.
	// { fallback: false } means other routes should 404.
	return { paths, fallback: true };
}

export const getStaticProps = async ({ params }) => {
	const posts = await (await fetch(`${URL}/${params.id}`)).json();
	const img = await (
		await fetch('https://dog.ceo/api/breeds/image/random')
	).json();
	console.log('[ID]: getStaticProps');

	return {
		props: {
			posts,
			img,
		},
		revalidate: 1,
	};
};

export default Post;
